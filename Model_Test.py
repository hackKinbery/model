#!/usr/bin/env python
# coding: utf-8

# In[ ]:


r = requests.get('http://104.248.102.232:8000/api/childRoutes/getStationByRouteId/{}'.format(18))

aaa=[]
for el in r.json():
    aaa.append ([float(el['lat']),float(el['long'])])


# In[142]:


import numpy as np
sss=0
for num in range(len(aaa)-1):
    sss+=np.sqrt((aaa[num][0]-aaa[num+1][0])**2+(aaa[num][1]-aaa[num+1][1])**2)
xxx=np.linspace(0,sss,20)


# In[421]:


def dlin (m0,m1):
    return np.sqrt((m0[0]-m1[0])**2+(m0[1]-m1[1])**2)

def get_pos(time):
    tp=1
    tm1=time
    ndl=dlin(aaa[0],aaa[1])
    while (ndl<tm1) & (tp<len(aaa)):
        tm1-=ndl
        tp+=1
        ndl=dlin(aaa[tp-1],aaa[tp])
    r0=(aaa[tp][0]-aaa[tp-1][0])/ndl*tm1+aaa[tp-1][0]
    r1=(aaa[tp][1]-aaa[tp-1][1])/ndl*tm1+aaa[tp-1][1]
    return [r0+np.random.rand()/500,r1+np.random.rand()/500,(np.random.rand()/1000)/sss*20]


# In[422]:


def get_mas():
    t1=(np.random.rand()/500)/sss*20
    tt=20+np.int32(np.round(t1,0))
    hh=14
    if tt>59:
        tt-=60
        hh+=1
    unixtime = time.mktime(datetime(2021, 12, 2, hh, tt).timetuple())

    mas1=[[aaa[0][0],aaa[0][1],unixtime]]
    for el in xxx:
        x1,y1,t2=get_pos(el)
        tt=20+np.int32(np.round(el/sss*20+t1+t2,0))
        hh=14
        if tt>59:
            tt-=60
            hh+=1
        unixtime = time.mktime(datetime(2021, 12, 3, hh, tt).timetuple())
        mas1.append([x1,y1,unixtime])
        t1+=t2
    return mas1


# In[423]:


def find_cell(mas):
    return np.array([np.argmin(np.sum((np.array(aaa)-np.array(x[0:2]))**2,axis=1)) for x in mas]).reshape(-1,1)


# In[424]:


def add_time(mmm):
    nn=mmm[0][2]
    return np.hstack(((np.array(mas)[:,2]-nn).reshape(-1,1),np.round(np.array(mas)[:,0:2],2),find_cell(mas)))


# In[426]:


from sklearn.model_selection import train_test_split
from sklearn import metrics
from sklearn.metrics import roc_curve
from sklearn.ensemble import RandomForestClassifier as RFC
from sklearn.ensemble import GradientBoostingClassifier as GBC
from sklearn.linear_model import LogisticRegression as LR
from sklearn.linear_model import LinearRegression as LnR
from sklearn.neighbors import KNeighborsClassifier as KNC
from matplotlib import pyplot as plt


# In[429]:


fin_mas=add_time(get_mas())
for x in range(10000):
    fin_mas=np.vstack((fin_mas,add_time(get_mas())))
Y=np.int32(fin_mas[:,-1])
X=fin_mas[:,:-1]
(X_train,X_test,y_trainN,y_testN)=train_test_split(X,Y,test_size=0.3,random_state=0)


# In[431]:


model_r=KNC(n_neighbors=10)
model_r.fit(X_train,y_trainN)
yp=model_r.predict(X_test)

plt.figure(figsize=(10,7))
plt.scatter(y_testN,yp)
plt.xlabel('Фактические данные')
plt.xlabel('Прогнозные данные')
plt.show()


# In[452]:


for xx in [0,2,3,4,5,6,7,8,10]:
    print('---------')
    y_train=(y_trainN==xx)*1
    y_test=(y_testN==xx)*1
    model_r=KNC(n_neighbors=5)
    model_r.fit(X_train,y_train)
    yp=model_r.predict_proba(X_test)
    fpr, tpr, thresholds = roc_curve(y_test, yp.T[1])
    plt.title(str(xx)+' Станция')
    plt.plot(fpr,tpr)
    plt.show()

    mm=thresholds[np.argmin((1-tpr[1:])**2+fpr[1:]**2)+1]
    print ('Порог: ',mm)
    print ('Accuracy: ',metrics.accuracy_score(y_test,np.int32(yp.T[1]>mm)))
    print ('Precision: ',metrics.precision_score(y_test,np.int32(yp.T[1]>mm)))
    print ('Recall: ',metrics.recall_score(y_test,np.int32(yp.T[1]>mm)))
    print ('F1: ',metrics.f1_score(y_test,np.int32(yp.T[1]>mm)))
    print ('ROC-AUC: ',metrics.roc_auc_score(y_test,np.int32(yp.T[1]>mm)))
    


# In[ ]:




