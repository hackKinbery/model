#!/usr/bin/env python
# coding: utf-8

# In[131]:


def sher_dash(user,port=8150):
    import dash_html_components as html
    from dash import callback_context
    import dash_leaflet as dl
    from dash import Dash
    from dash.dependencies import Input, Output
    import dash_core_components as dcc
    import numpy as np
    import datetime
    import json, requests
    import warnings
    warnings.filterwarnings("ignore")

    
    c1=57.73660
    c2=40.91485
    Sher_coordinates = (c1, c2)
    res=[]
    route_name='Name'

    interval1=dcc.Interval(
                id='interval-component',
                interval=1*1000, # in milliseconds
                n_intervals=0
            )



    timer = html.Div(
        html.Div([
            html.H1(children='The time is: '+str(datetime.datetime.now()) ,id='H1'),
            interval1
        ])
    )

    buttoms=  html.Div([html.Button('Save', id='btn1', n_clicks=0),
                        html.Button('Clear', id='btn2', n_clicks=0),
                        dcc.Input(id='input_name',value='Name'),
                       ])  

    

    iconUrl = "https://dash-leaflet.herokuapp.com/assets/icon_plane.png"
    def get_info(sam,pos):
        header = [html.H5("Самолет " + sam),html.B( '{:.5f} / {:.5f}'.format(pos[0],pos[1]))]
        return header


    samolet=dl.Marker(draggable=True,id="A380/5",
                  position=[c1, c2],
                  icon=dict(iconUrl=iconUrl, iconAnchor=[10, 10]),
                 children=dl.Tooltip(get_info('A380/5',[55.974182430748066, 37.416772842407234])))

    lll=[]
    lines=html.Div(lll,id='polilines')

    

    external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
    app = Dash(prevent_initial_callbacks=True,external_stylesheets=external_stylesheets)
    app.layout = html.Div([timer,buttoms,
        dl.Map([dl.TileLayer(), 
           html.Div([samolet]), lines,
           dl.LayerGroup(id="layer"), dl.LayerGroup(id="layer_pol")],
           id="map", center=Sher_coordinates, zoom=15,
           style={'width': '100%', 'height': '100vh', 'margin': "auto", "display": "block"}),
    ])
    
    
    
    @app.callback(Output('H1', 'children'),
              Input('interval-component', 'n_intervals'))
    def update_metrics(n):
        return str(n)+': The time is: ' + str(datetime.datetime.now())

    


    @app.callback(Output('A380/5', "children"),
              Input('A380/5', "position"),Input('A380/5', "id"))
    def update_samolet(pos,idd):
        return dl.Tooltip(get_info(idd,pos))




    @app.callback(
        Output('polilines', 'children'),
        Input('btn1', 'n_clicks'),
        Input('btn2', 'n_clicks'),
        Input("map", "click_lat_lng"),
        Input("input_name", "value"),
   )
    def displayClick(btn1,btn2,click_lat_lng,iv):
        nonlocal res
        nonlocal buttoms
        nonlocal user
        nonlocal route_name
        cb=callback_context.triggered[0]['prop_id']
        if cb=='btn1.n_clicks':
            p1=user
            p2=route_name
            r = requests.get('http://104.248.102.232:8000/api/childRoutes/saveNewRoute/{}/{}'.format(p1,p2))
            route=int(r.json()['id'])
            for num,el in enumerate(res):
                requests.get('http://104.248.102.232:8000/api/childRoutes/saveStation/{}/{}/{}/{}'.format(route,el[0],el[1],num))

                
            
            res=[]
            res1=[]
        elif cb=='btn2.n_clicks':
            res=[]
            res1=[]
        elif cb=='input_name.value':
            res1=[dl.Polyline(positions=res)]
            route_name=iv
        else:
            res.append(click_lat_lng)
            res1=[dl.Polyline(positions=res)]
        return res1




    if __name__ == '__main__':
        app.run_server(debug=False, port=port)


# In[ ]:


sher_dash(3)


# In[ ]:




